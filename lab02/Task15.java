/*
С клавиатуры вводятся натуральные числа X и Y.
Вычислить произведение x y, используя лишь операцию сложения.
*/

import java.util.Scanner;

public class Task15 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите число X:");
        int x = scan.nextInt();
        System.out.println("Введите число Y:");
        int y = scan.nextInt();

        int result = 0;
        for (int i = 1; i <= y; i++) {
            result += x;
        }

        System.out.printf("%s * %s = %s \n", x, y, result);
    }
}