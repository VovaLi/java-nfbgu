/******************************************************************************

Транслитиратор. Из консоли вводится строка на русском языке, перевести ее в транслит.

*******************************************************************************/

import java.util.Map;
import static java.util.Map.entry;
import java.util.Scanner;

public class Task26 {
    public static void main(String[] args) {
        Map < String, String > engRus = Map.ofEntries(
            entry("а", "a"), entry("б", "b"), entry("в", "v"), entry("г", "g"), entry("д", "d"), entry("е", "e"), entry("ё", "io"), entry("ж", "zh"), entry("з", "z"), entry("и", "i"), entry("й", "y"), entry("к", "k"), entry("л", "l"), entry("м", "m"), entry("н", "n"), entry("о", "o"), entry("п", "p"), entry("р", "r"), entry("с", "s"), entry("т", "t"), entry("у", "u"), entry("ф", "f"), entry("х", "h"), entry("ц", "c"), entry("ч", "ch"), entry("ш", "sh"), entry("щ", "jsh"), entry("ъ", "hh"), entry("ы", "ih"), entry("ь", "'"), entry("э", "eh"), entry("ю", "ju"), entry("я", "ja"), entry("А", "A"), entry("Б", "B"), entry("В", "V"), entry("Г", "G"), entry("Д", "D"), entry("Е", "E"), entry("Ё", "IO"), entry("Ж", "ZH"), entry("З", "Z"), entry("И", "I"), entry("Й", "Y"), entry("К", "K"), entry("Л", "L"), entry("М", "M"), entry("Н", "N"), entry("О", "O"), entry("П", "P"), entry("Р", "R"), entry("С", "S"), entry("Т", "T"), entry("У", "U"), entry("Ф", "F"), entry("Х", "H"), entry("Ц", "C"), entry("Ч", "CH"), entry("Ш", "SH"), entry("Щ", "JSH"), entry("Ъ", "HH"), entry("Ы", "IH"), entry("Ь", "'"), entry("Э", "EH"), entry("Ю", "JU"), entry("Я", "JA"), entry(" ", " ")
        );

        Scanner scan = new Scanner(System.in);
        System.out.print("Введите текст: ");
        String str = scan.nextLine();

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            if (null == engRus.get(str.substring(i, i + 1))) {
                result.append(' ');
            } else {
                result.append(engRus.get(str.substring(i, i + 1)));
            }
        };

        System.out.println(result);
    }
}