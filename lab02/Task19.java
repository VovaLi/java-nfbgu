package lab02;

import java.util.Scanner;

public class Task19 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите сторону А :");
        int a = scan.nextInt();
        System.out.println("Введите сторону В :");
        int b = scan.nextInt();
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                if (i == 0 || i == a - 1) {
                    System.out.print("*");
                } else {
                    if (j == 0 || j == b - 1) {
                        System.out.print('*');
                    } else {
                        System.out.print(' ');
                    }
                }

            }
            System.out.print("\n\r");
        }

    }

}
