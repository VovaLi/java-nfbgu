/*
8. С клавиатуры вводится число - N, 
необходимо написать программу, 
которая выводит таблицу умножения чисел от 1 до 10 на заданное число.
*/

package lab02;
import java.util.Scanner;

public class Task08 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите число");
        int num = scan.nextInt();
        for (int i = 1; i <= 10; i++) {
            System.out.printf("%s * %s = %s \n", num, i, num * i);
        }
    }
}