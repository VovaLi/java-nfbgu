package lab6;

public class Task {
    public static void main(String[] args) {
        DoublyLinkedList<String> myList = new DoublyLinkedList<>();
        myList.put("Hello");
        myList.put("this");
        myList.put("is");
        myList.put("program");
        for (int i = 0; i < myList.length(); i++) {
            System.out.println(myList.get(i));
        }
        System.out.println("Размера списка: " + myList.length());
        myList.remove(0);
        for (int i = 0; i < myList.length(); i++) {
            System.out.println(myList.get(i));
        }
        System.out.println("Размера списка: " + myList.length());
    }
}
