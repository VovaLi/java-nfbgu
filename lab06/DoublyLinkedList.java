package lab6;

public class DoublyLinkedList<T> implements List<T> {

    private Node<T> rootNode;
    private Node<T> headNode;
    private int size = 0;

    private static class Node<T> {
        private final T inf;
        Node<T> prev;
        Node<T> next;

        Node(Node<T> prev, Node<T> next, T inf) {
            this.inf = inf;
            this.prev = prev;
            this.next = next;
        }
    }

    @Override
    public T get(int i) {
        check(i);
        Node<T> temp = rootNode;
        for (int k = 0; k < i; k++) {
            temp = temp.next;
        }
        return temp.inf;
    }

    @Override
    public void put(T e) {
        if (rootNode == null) {
            rootNode = new Node<>(null, null, e);
            headNode = rootNode;
        } else {
            headNode.next = new Node<>(headNode, headNode.next, e);
            headNode = headNode.next;
        }
        size++;
    }

    @Override
    public void put(int i, T e) {
        check(i - 1);
        if (i == 0) {
            rootNode.prev = new Node<T>(null, rootNode, e);
            rootNode = rootNode.prev;
            size++;
            return;
        }
        if (i == size) {
            headNode.next = new Node<T>(headNode, null, e);
            headNode = headNode.next;
            size++;
            return;
        }
        Node<T> temp = rootNode;
        for (int k = 0; k < i - 1; k++) {
            temp = temp.next;
        }
        temp.next = new Node<T>(temp, temp.next, e);
        temp.next.next.prev = temp.next;
        size++;
    }

    private void check(int i) {
        if (i >= size || i < 0) {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public void remove(int i) {
        check(i);
        if (i == 0) {
            rootNode = rootNode.next;
            rootNode.prev = null;
            size--;
            return;
        }
        if (i == this.size - 1) {
            headNode = headNode.prev;
            headNode.next = null;
            size--;
            return;
        }
        Node<T> temp = rootNode;
        for (int k = 0; k < i - 1; k++) {
            temp = temp.next;
        }
        temp.next = temp.next.next;
        temp.next.prev = temp;
        size--;
    }

    @Override
    public void remove(T e) {
        Node<T> temp = rootNode;
        if (rootNode.inf.equals(e)) {
            rootNode = rootNode.next;
            rootNode.prev = null;
            size--;
            return;
        }
        if (headNode.inf.equals(e)) {
            headNode = headNode.prev;
            headNode.next = null;
            size--;
            return;
        }
        for (int i = 0; i < size - 1; i++) {
            if (temp.next.inf.equals(e)) {
                temp.next = temp.next.next;
                temp.next.prev = temp;
                size--;
                return;
            }
            temp = temp.next;
        }
    }

    @Override
    public int length() {
        return size;
    }
}
