package lab5;

import java.util.Objects;

public class Pair<T, E> {
    private final T i;
    private final E s;

    private Pair(T i, E s) {
        this.i = i;
        this.s = s;
    }

    public T getFirst() {
        return i;
    }

    public E getSecond() {
        return s;
    }


    public static <T, E> Pair<T, E> of(T s, E i) {
        return new Pair<>(s, i);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(i, pair.i) && Objects.equals(s, pair.s);
    }

    @Override
    public int hashCode() {
        return Objects.hash(i, s);
    }
}
