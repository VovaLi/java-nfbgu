package lab5;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Task3 {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("1", 1);
        List<Exception> list = new LinkedList<Exception>();
        list.add(new Exception());
        list.add(new IOException());
        Pair<Map<String, Integer>, List<Exception>> pair = Pair.of(map, list);
    }
}