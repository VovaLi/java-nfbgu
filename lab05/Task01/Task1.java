package lab05;

public class Task1 {
    public static double sqrt(double x) {
        if (x < 0) throw new IllegalArgumentException("Expected non-negative number, got " + x);
        return Math.sqrt(x);
    }

    public static void main(String[] args) {
        try {
            System.out.print(sqrt(-81));
        } catch (IllegalArgumentException e) {
            System.out.print(e.getMessage());
        }
    }
}