package lab8;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;

public class Task {
    public static void main(String[] args) {
        try (FileWriter fw = new FileWriter("writer.txt", true)) {
            int a = requestNumber("Введите начало диапазона поожительных чисел: ");
            int b = requestNumber("Введите конец диапазона поожительных чисел: ");
            for (int i = 0; i < 50; i++) {
                int c = a + (int)(Math.random() * b);
                fw.write(c + " ");
            }
            a = requestNumber("Введите начало диапазона отрицательных чисел: ");
            b = requestNumber("Введите конец диапазона отрицательных чисел: ");
            for (int i = 0; i < 50; i++) {
                int c = a + (int)(Math.random() * b);
                fw.write(c + " ");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (FileReader fr = new FileReader("writer.txt"); FileWriter fwp = new FileWriter("pos.txt", true); FileWriter fwn = new FileWriter("neg.txt", true)) {
            Scanner scan = new Scanner(fr);
            String numbers = scan.nextLine().trim();
            String[] arr = numbers.split(" ");
            int[] numbersArr = new int[arr.length];
            for (int i = 0; i < arr.length; i++) {
                numbersArr[i] = Integer.parseInt(arr[i]);
            }
            for (int num: numbersArr) {
                if (num > 0) {
                    fwp.write(num + " ");
                } else {
                    fwn.write(num + " ");
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    static int requestNumber(String str) {
        Scanner scanner = new Scanner(System.in);
        System.out.print(str);
        return scanner.nextInt();
    }
}