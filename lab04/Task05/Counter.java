package lab04;

import java.util.Random;

public class Counter {
    private final int from;
    private final int to;
    private int c;
    private static final int DEFAULT_VALUE = 0;
    Random random = new Random();


    Counter(int from, int to, boolean isRandom) {
        this.from = from;
        this.to = to;
        this.c = isRandom ? random.nextInt(to - from) + from : DEFAULT_VALUE;
    }

    public int getC() {
        return c;
    }


    public void increase() {
        if (this.c < this.to) {
            this.c += 1;
        }
    }

    public void decrease() {
        if (this.c > this.from) {
            this.c -= 1;
        }
    }
}
