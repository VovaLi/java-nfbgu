package lab04;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        boolean isRandom = requestBoolean();
        int from = requestNum("Введите начало диапазона: ");
        int to = requestNum("Введите конец диапазона: ");
        Counter c = new Counter(from, to, isRandom);
        System.out.println("Начальное значение c: " + c.getC());
        c.increase();
        c.increase();
        c.decrease();
        System.out.println("Конечное значение c: " + c.getC());
    }

    static boolean requestBoolean() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите true/false: ");
        return scanner.nextBoolean();
    }

    static int requestNum(String str) {
        Scanner scanner = new Scanner(System.in);
        System.out.print(str);
        return scanner.nextInt();
    }

}
