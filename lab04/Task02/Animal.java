package lab4;

public abstract class Animal {
    private final String food;
    private final String location;

    Animal(String food, String location) {
        this.food = food;
        this.location = location;
    }

    public String getFood() {
        return food;
    }

    public String getLocation() {
        return location;
    }

    public abstract String makeNoise();

    public abstract String eat();

    public String sleep() {
        return "Животное спит";
    }
}
