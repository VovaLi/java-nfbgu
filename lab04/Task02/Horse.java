package lab4;

public class Horse extends Animal {
    private final int weight;

    Horse(String food, String location, int weight) {
        super(food, location);
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public String makeNoise() {
        return "Лошадь ржет";
    }

    @Override
    public String eat() {
        return "Лошадь ест" + this.getFood();
    }
}
