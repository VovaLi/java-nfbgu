package lab4;

public class Task2 {
    public static void main(String[] args) {
        Animal[] animals = new Animal[3];
        animals[0] = new Cat("Корм", "Дом", "Белый");
        animals[1] = new Dog("Еда", "Двор", "Бульдог");
        animals[2] = new Horse("Трава", "Сарай", 150);
        Veterinarian aibolit = new Veterinarian();
        for (Animal animal : animals) {
            aibolit.treatAnimal(animal);
        }
    }
}
