package lab4;

public class Cat extends Animal {
    private final String color;

    Cat(String food, String location, String color) {
        super(food, location);
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String makeNoise() {
        return "Кот мяукает";
    }

    @Override
    public String eat() {
        return "Кот ест" + this.getFood();
    }
}
