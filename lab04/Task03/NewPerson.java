package lab4;

public class NewPerson implements Treatable {
    private final String name;

    NewPerson(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void treat() {
        System.out.print("На приеме " + this.getName() + ". ");
        System.out.println("Идет лечение человека.");
    }
}