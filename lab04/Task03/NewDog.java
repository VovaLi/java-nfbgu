package lab4;

public class NewDog implements Treatable {
    private final String breed;

    NewDog(String breed) {
        this.breed = breed;
    }

    public String getBreed() {
        return breed;
    }

    public void treat() {
        System.out.print("На приеме собака породы " + this.getBreed() + ". ");
        System.out.println("Лечение собаки.");
    }
}