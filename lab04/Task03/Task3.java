package lab4;

public class Task3 {
    public static void main(String[] args) {
        Treatable person = new NewPerson("Иван");
        Treatable dog = new NewDog("овчарка");
        Treatable cat = new NewCat("белый");
        Treatable horse = new NewHorse(150);
        Treatable[] creations = {person, dog, cat, horse};
        Doctor doctor = new Doctor();
        doctor.treatCreations(creations);
    }
}
