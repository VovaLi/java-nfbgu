package lab4;

public class NewCat implements Treatable {
    private final String color;

    NewCat(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void treat() {
        System.out.print("На приеме кот следующего цвета - " + this.getColor() + ". ");
        System.out.println("Лечение кота.");
    }
}