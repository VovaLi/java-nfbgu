package lab4;

public class Doctor {
    public void treatCreations(Treatable[] creations) {
        for (Treatable creation :
                creations) {
            creation.treat();
        }
    }
}