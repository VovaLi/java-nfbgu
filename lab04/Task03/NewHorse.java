package lab4;

public class NewHorse implements Treatable {
    private final int weight;

    NewHorse(int weight) {
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public void treat() {
        System.out.print("На приеме лошадь весом " + this.getWeight() + "кг. ");
        System.out.println("Лечение лошади.");
    }
}