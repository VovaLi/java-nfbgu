package lab04;

import java.util.Objects;

public class AnotherNewPerson {
    private final String name;
    private final Gender gender;
    private final ClothingSize size;

    AnotherNewPerson(String name, Gender gender, ClothingSize size) {
        this.name = name;
        this.gender = gender;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender.getGender();
    }

    public int getSize() {
        return size.getSize();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        AnotherNewPerson a = (AnotherNewPerson) obj;
        return name.equals(a.name) && gender.equals(a.gender) && size == a.size;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, size, gender);
    }
}
