package lab04;

public enum ClothingSize {
    XS(46),
    S(48),
    M(50),
    L(52),
    XL(54),
    XXL(56);
    private final int size;

    ClothingSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
