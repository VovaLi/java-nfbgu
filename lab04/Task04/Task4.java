package lab04;

import java.util.Objects;

public class Task4 {
    public static void main(String[] args) {
        AnotherNewPerson[] persons = new AnotherNewPerson[2];
        persons[0] = new AnotherNewPerson("Ваня", Gender.MALE,
                ClothingSize.S);
        persons[1] = new AnotherNewPerson("Маша", Gender.FEMALE,
                ClothingSize.XS);
        for (AnotherNewPerson person :
                persons) {
            System.out.println("Имя: " + person.getName());
            System.out.println("Пол: " + person.getGender());
            System.out.println("Размер одежды: " + person.getSize());
            System.out.println(" ");
        }
    }
}
