package lab04;

public enum Gender {
    MALE("Муж"),
    FEMALE("Жен");
    private final String gender;

    Gender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }
}
