/*
Автомобили Создать класс Car и Driver.
Класс Driver содержит поля - ФИО, стаж вождения. Класс Engine содержит поля - мощность, производитель. 
Класс Car содержит поля - марка автомобиля, класс автомобиля, вес, водитель типа Driver, мотор типа Engine. 
Методы start(), stop(), turnRight(), turnLeft(), которые выводят на печать: "Поехали", "Останавливаемся", "Поворот направо" или "Поворот налево". 
А также метод toString(), который выводит полную информацию об автомобиле, ее водителе и моторе. 
Создать производный от Car класс - Lorry (грузовик), характеризуемый также грузоподъемностью кузова. 
Создать производный от Car класс - SportCar, характеризуемый также предельной скоростью. Пусть класс Driver расширяет класс Person.
*/
package lab4;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите ФИО и стаж водителя: ");
        Driver driver = new Driver(input.nextLine(), input.nextInt());
        System.out.println("Введите мощность и производителя машины: ");
        Engine engine = new Engine(input.nextDouble(), input.nextLine());
        System.out.println("Введите марку, класс и вес машины: ");
        Car car = new Car(input.nextLine(), input.nextLine(), input.nextInt(), driver, engine);
        System.out.println(car.toString());
        car.start();
        car.turnRight();
        car.turnLeft();
        car.stop();
    }
}
