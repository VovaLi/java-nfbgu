package lab3;

class Triangle extends Figure {
    private double a;
    private double b;
    private double c;

    Triangle(double[] a) {
        this.a = a[0];
        this.b = a[1];
        this.c = a[2];
    }

    public double calcPerimeter() {
        return a + b + c;
    }

    public double calcArea() {
        double o = (a + b + c) / 2;
        return (double) Math.sqrt(o * (o - a) * (o - b) * (o - c));
    }
}