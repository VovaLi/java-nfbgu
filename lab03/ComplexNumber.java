/*
Дан класс ComplexNumber. 
Переопределите в нем методы equals() и hashCode() так, чтобы equals() сравнивал экземпляры ComplexNumber по содержимому полей re и im, а hashCode() был бы согласованным с реализацией equals().
*/

package lab3;

public final class ComplexNumber {
    private final Double re;
    private final Double im;

    public ComplexNumber(double re, double im) {
        this.re = re;
        this.im = im;
    }

    public double getRe() {
        return re;
    }

    public double getIm() {
        return im;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        ComplexNumber n = (ComplexNumber) obj;
        return re.equals(n.re) && im.equals(n.im);
    }

    public int hashCode() {
        int res1 = this.re.hashCode();
        int res2 = this.im.hashCode();
        return res1 + res2;
    }
}