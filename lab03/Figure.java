package lab3;

public abstract class Figure {

    public abstract double calcPerimeter();

    public abstract double calcArea();

    public static double summa(Figure[] s){
        double area = 0;
        for (Figure l : s) {
            area += l.calcArea();
        }
        return area;
    }
}
