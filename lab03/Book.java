package lab3;

class Book {
    private final int bookPages;

    public Book(int bookPages) {
        this.bookPages = bookPages;
    }

    public int getPages() {
        return bookPages;
    }
}
