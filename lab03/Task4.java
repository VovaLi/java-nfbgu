package lab3;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
		
        System.out.print("Введите радиус первой окружности: ");
        double r1 = input.nextDouble();
        Circle cir1 = new Circle(r1);
		
        System.out.print("Введите радиус второй окружности: ");
        double r2 = input.nextDouble();
        Circle cir2 = new Circle(r2);
		
        System.out.println("Площадь окружности: " + cir1.circleArea());
        cir1.position(1, 2);
        System.out.println("Входит ли окружность - " + cir1.enter(cir2));
        System.out.println("Расстояние между центрами окружностей: " + cir1.distance(cir2));
    }
}