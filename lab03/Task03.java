/*
3.  Напишите рекурсивный метод вычисляющий n-ое число Фибоначчи
*/

package lab3;

import java.util.Scanner;

public class Task03 {
    static int requestInt() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }


    public static void main(String[] args) {
        int n = requestInt();
        System.out.println(fibo(n));
    }

    static int fibo(int n) {
        if (n == 1 || n == 2) {
            return 1;
        }
        return fibo(n - 1) + fibo(n - 2);
    }
}
