/*
2.  Написать класс Библиотека (в библиотеке хранятся книги, например в массиве) используя класс Книга. Книга характеризуется количеством страниц. Класс Библиотека должен предоставлять информацию об общем количестве страниц всех книг и среднем кол-ве страниц на книгу
*/

package lab3;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Количество книг: ");
        int cBooks = input.nextInt();
        Library allBooks = new Library(cBooks);
        System.out.println("Введите количество страниц каждой книги:");
        for (int i = 0; i < cBooks; i++) {
            allBooks.addBook(new Book(input.nextInt()));
        }
        System.out.println("Общее количество страниц: " + allBooks.countPages());
        System.out.println("Среднее количество страниц: " + allBooks.countAverage());
    }
}
