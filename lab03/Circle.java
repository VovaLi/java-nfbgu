package lab3;

class Circle {
    private double r;
    private double x = 0;
    private double y = 0;

    public Circle(double r) {
        this.r = r;
    }

    public double circleArea() {
        return Math.PI * Math.pow(r, 2);
    }

    public void position(double deltax, double deltay) {
        this.x += deltax;
        this.y += deltay;
    }

    public boolean enter(Circle cir2) {
        return (Math.pow((this.x - cir2.x), 2) + Math.pow((this.y - cir2.y), 2)) <= Math.pow((this.r - cir2.r), 2);
    }

    public double distance(Circle cir2) {
        return Math.sqrt(Math.pow((this.x - cir2.x), 2) + Math.pow((this.y - cir2.y), 2));
    }
}