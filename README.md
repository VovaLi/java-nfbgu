# Памятка для себя

## Запуск и компиляция в консоли

Компиляция:
```
"C:\Program Files\Java\jdk-18.0.2.1\bin\javac" Name.java
```

Запуск:
```
"C:\Program Files\Java\jdk-18.0.2.1\bin\java" Name
```

Для того, чтобы не писать полный путь каждый раз необходимо изменить системную переменную среды JAVA_HOME (Для windows). Более подробную инструкцию прочитать в интернете.

После данных манипуляций можно использовать команды java и javac.

НЕ ИСПОЛЬЗОВАТЬ IDE!!! ConEmu + Текстовый редактор

Немного команд из ненавистной windows:

md или mkdir ИМЯ - стандартная команда для создания папок в консоли

dir - дерево

NUL ИМЯ - создание файла

del ИМЯ - удаление файла

## Работа с git
```
git add .
git init
git commit -m ''
git push -uf origin main

```